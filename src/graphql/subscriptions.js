/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateBookOrder = /* GraphQL */ `
  subscription OnCreateBookOrder(
    $filter: ModelSubscriptionBookOrderFilterInput
  ) {
    onCreateBookOrder(filter: $filter) {
      id
      bookId
      orderId
      order {
        id
        user
        date
        total
        createdAt
        updatedAt
        __typename
      }
      createdAt
      updatedAt
      orderBookOrdersId
      __typename
    }
  }
`;
export const onCreateOrder = /* GraphQL */ `
  subscription OnCreateOrder(
    $filter: ModelSubscriptionOrderFilterInput
    $user: String
  ) {
    onCreateOrder(filter: $filter, user: $user) {
      id
      user
      date
      total
      bookOrders {
        nextToken
        __typename
      }
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const onUpdateOrder = /* GraphQL */ `
  subscription OnUpdateOrder(
    $filter: ModelSubscriptionOrderFilterInput
    $user: String
  ) {
    onUpdateOrder(filter: $filter, user: $user) {
      id
      user
      date
      total
      bookOrders {
        nextToken
        __typename
      }
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const onDeleteOrder = /* GraphQL */ `
  subscription OnDeleteOrder(
    $filter: ModelSubscriptionOrderFilterInput
    $user: String
  ) {
    onDeleteOrder(filter: $filter, user: $user) {
      id
      user
      date
      total
      bookOrders {
        nextToken
        __typename
      }
      createdAt
      updatedAt
      __typename
    }
  }
`;
