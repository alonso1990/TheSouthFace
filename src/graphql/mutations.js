/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createBookOrder = /* GraphQL */ `
  mutation CreateBookOrder(
    $input: CreateBookOrderInput!
    $condition: ModelBookOrderConditionInput
  ) {
    createBookOrder(input: $input, condition: $condition) {
      id
      bookId
      orderId
      order {
        id
        user
        date
        total
        createdAt
        updatedAt
        __typename
      }
      createdAt
      updatedAt
      orderBookOrdersId
      __typename
    }
  }
`;
export const createorder = /* GraphQL */ `
  mutation Createorder(
    $input: CreateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    createorder(input: $input, condition: $condition) {
      id
      user
      date
      total
      bookOrders {
        nextToken
        __typename
      }
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const updateOrder = /* GraphQL */ `
  mutation UpdateOrder(
    $input: UpdateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    updateOrder(input: $input, condition: $condition) {
      id
      user
      date
      total
      bookOrders {
        nextToken
        __typename
      }
      createdAt
      updatedAt
      __typename
    }
  }
`;
export const deleteOrder = /* GraphQL */ `
  mutation DeleteOrder(
    $input: DeleteOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    deleteOrder(input: $input, condition: $condition) {
      id
      user
      date
      total
      bookOrders {
        nextToken
        __typename
      }
      createdAt
      updatedAt
      __typename
    }
  }
`;
